---
title: Screenshots
menu:
  main:
    parent: res
    weight: 2
screenshots:
- url: /img/screenshots/kstars_main.png
  name: "KStars Primary Window"
  link: /img/screenshots/kstars_main.png
- url: /img/screenshots/kstars_billionsandbillions.png
  name: "Billions and Billions: 100 million stars using USNO Nomad Catalog"
  link: /img/screenshots/kstars_billionsandbillions.png
- url: /img/screenshots/kstars_popup.png
  name: "Popup menu"
  link: /img/screenshots/kstars_popup.png
- url: /img/screenshots/kstars_detaildialog.png
  name: "Detail dialog: Examining Detailed info for M42"
  link: /img/screenshots/kstars_detaildialog.png
- url: /img/screenshots/kstars_calculator.png
  name: "Astrocalculator"
  link: /img/screenshots/kstars_calculator.png
- url: /img/screenshots/kstars_calender.png
  name: "Sky Calender"
  link: /img/screenshots/kstars_calender.png
- url: /img/screenshots/kstars_ekos.png
  name: "Ekos Astrophotography Tool"
  link: /img/screenshots/kstars_ekos.png
- url: /img/screenshots/kstars_fov_editor.png
  name: "FOV Editor"
  link: /img/screenshots/kstars_fov_editor.png
- url: /img/screenshots/kstars_observationplanner.png
  name: "Observation Planner: With Altitude vs. Time tool"
  link: /img/screenshots/kstars_observationplanner.png
- url: /img/screenshots/kstars_solarsystem.png
  name: "Solar System Tool: With Jupiter moon tool"
  link: /img/screenshots/kstars_solarsystem.png
- url: /img/screenshots/kstars_ghns.png
  name: "Get More Data Online"
  link: /img/screenshots/kstars_ghns.png
- url: /img/screenshots/kstars_whatsuptonight.png
  name: "What is up tonight tool"
  link: /img/screenshots/kstars_whatsuptonight.png
- url: /img/screenshots/kstars_mac_constellation.png
  name: "Constellation Art"
  link: /img/screenshots/kstars_mac_constellation.png
- url: /img/screenshots/kstars_mac_galaxy.png
  name: "Explore Galaxy with What is Interesting"
  link: /img/screenshots/kstars_mac_galaxy.png
- url: /img/screenshots/kstars_mac_wit.png
  name: "What is Interesting Tool"
  link: /img/screenshots/kstars_mac_wit.png
---

{{< screenshots name="screenshots" centered="false" >}}
