---
layout: users
title: User Support
menu:
  main:
    weight: 3
name: KStars
forum: https://forum.kde.org/
handbook: https://docs.kde.org/trunk5/en/kstars/kstars/index.html
---
## Other resources
- [KStars Deep Sky Catalogs](/catalogs) — Catalogs documentation
- [Ekos](https://www.indilib.org/about/ekos.html) — Complete astrophotography and observatory control framework
- [INDI](https://www.indilib.org/index.php?title=Main_Page) — protocol to operate astronomical instruments

### Check out these other free and open source astronomy software
- [Stellarium](https://www.stellarium.org/): Stellarium has the most aesthetic rendering of the sky map. Ideal for giving public shows. Has a larger star catalog than KStars. It can distort images for projection on fish-eye planetarium projectors.
- [SkyChart (Cartes du Ciel)](https://www.ap-i.net/skychart/): Cartes du Ciel has the largest database of astronomical objects for any free software. It has support for the very very extensive star catalog, PPMXL. Also has tons of other downloadable catalogs. 
