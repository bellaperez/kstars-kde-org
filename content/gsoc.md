---
title: 'Google Summer of Code'
menu:
  main:
    parent: res
    weight: 5
---

### Welcome prospective students!
KStars welcomes all prospective students planning on participating in [GSoC 2022](https://developers.google.com/open-source/gsoc/).

By participating in Open Source projects such as KStars, you shall gain a considerable experience not only in terms of real world development practices in the software industry, but also improve your skills in planning, management, and communication. There are usually three/four slots available for KStars, so the competition to win is stiff! After a student is selected, he/she shall implement the selected project over the course of roughly three months with mid-term evaluations submitted to Google on the progress of the project's objectives.

You are expected to fully dedicate the GSoC 2022 summer period to the development of KStars. Specifically, the following is **NOT** accepted during the program duration:

- Full/Part time job
- University summer course
- Internship
- Travel/Vacation

GSoC 2022 should be treated as a *full time* job by the prospective student for the duration of the program. If you have any college examinations during the GSoC 2022 period they must be clearly indicated in the proposal.

Before any student is selected for any KStars GSoC project, they must **demonstrate** their ability to work in KStars/KDE/Qt environment as discussed in the next section.

### Where do I start?

To develop for KStars, you are expected to be well versed in the following areas:

1. C/C++
1. Algorithms, data structures, and design patterns.
1. Qt/QML
1. KDE

Since you selected KStars for your project, it is safe to assume you are interested in astronomy; therefore, a background in Astronomy also helps in the development process. To prepare yourself for GSoC, perform the following steps:

1. Read [KStars GSoC ideas](https://community.kde.org/GSoC/2022/Ideas#KStars): The KStars team compiled a few ideas for GSoC students. You need to carefully read all ideas and select which one best suit your interests and abilities. If none match your interest, you can propose a new idea pending the approval of KStars mentor.
1. Prepare your computer environment for KStars development:
   - Download and install a modern Linux distribution: While you can build KStars on any Linux distribution, it is recommended to install a modern KDE-based distribution such as [Kubuntu](http://www.kubuntu.org/getkubuntu) 20.04 or later.
   - Download [Qt Creator](http://www.qt.io/download-open-source/): Ensure to install all examples and documentation. [Several examples](http://doc.qt.io/qt-5/qtexamplesandtutorials.html) are available online as well. Start with simple *Hello World* application to get familiar with Qt, and proceed with building GUI applications based on QWidget and QML.
   - [Build GIT version of KStars.](https://invent.kde.org/education/kstars#development) You should be able to open KStars project in Qt Creator. KDE applications using CMake build system. Go to File --> Open Project and select CMakelists.txt
1. Familiarize yourself with KStars: The best way to learn about KStars is to dig the code and fix/improve things! Start with simple ideas or bug fixes:
   - Study [KStars API](https://api.kde.org/kstars/html/index.html): KStars may be a complex piece of software but it's not magic, learn how it internally works.
   - Review the [KStars handbook](https://docs.kde.org/stable/en/kdeedu/kstars/): While the handbook is meant for end users, it's good to familiarize yourself with how end users utilize KStars.
   - Hunt [pending KStars bugs](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f6=status_whiteboard&f7=content&f8=CP&f9=CP&j1=OR&o2=substring&o3=substring&o4=substring&o5=substring&o6=substring&o7=matches&product=kstars&query_format=advanced&v2=kstars&v3=kstars&v4=kstars&v5=kstars&v6=kstars&v7=%22kstars%22): Try to fix pending KStars bugs items, send a patch to the maintainer and/or propose possible fixes!
   - Search [pending KStars issues](https://invent.kde.org/education/kstars/-/issues?label_name%5B%5D=Junior+Job): Check the list of issues in KStars. These can be bugs, or tasks, or new features to be implemented. Pick one issue and coordinate with the maintainer to work on it.
   - Check out INDI forum [Wishlist](https://indilib.org/forum/wish-list.html) items. Some are applicable to KStars/Ekos and could serve as an entry point to KStars and Ekos development.

Students are **required** to either implement one of junior jobs and/or resolve bugs to prove eligibility to participate in KStars. You need to be actively involved in KStars before the GSoC official proposal submission date and evaluation.

### Communication

During the course of the GSoC project, you are expected to keep your mentor and the KStars developers informed throughout the project cycle:

1. First and foremost is KStars Developement [mailing list](https://mail.kde.org/mailman/listinfo/kstars-devel): Use the mailing list for everything from inquiry, non-trivial code commits, and progress reports.
2. Developers sometimes hang out at [KStars web chat channel](https://webchat.kde.org/#/room/#kstars:kde.org). You can discuss a variety of topics with them subject to availability.

While students are strongly encouraged to ask any development related questions via any communication channel above, you are **expected** to do your research before you ask. *Repetitively* asking trivial questions to mentors shows you have not performed your research well into the issue or lack basic problem-solving abilities. Therefore, research your issues then ask specific direct questions to the mailing list or mentors.
