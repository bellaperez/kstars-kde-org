---
title: AstroInfo Project
menu:
  main:
    parent: res
    weight: 3
---
## The AstroInfo Project: The Everything2 of Astronomy

KStars is primarily a learning tool, to teach people about the wonder
of the night sky, our window to the universe. There is a **lot**
to learn, however, and the program alone can't do everything. We are
writing a collection of short articles to be included with KStars
to help explain the concepts behind KStars. There are a lot of articles
we'd like to include; many more than we can possibly write ourselves in a
reasonable amount of time.

If you are interested in contributing an article to AstroInfo, please
join us on the [kstars-info mailing list](https://lists.sourceforge.net/lists/listinfo/kstars-info).
The list is used for submission of new articles, and 
review of existing articles. Feel free to use plain text, HTML, or 
DocBook XML when writing or editing an article; we can convert
the article to DocBook XML from other formats.

You can read the current AstroInfo articles in the [KStars Handbook](https://docs.kde.org/trunk5/en/kstars/kstars/index.html), 
in the [AstroInfo Chapter](https://docs.kde.org/trunk5/en/kstars/kstars/astroinfo.html).
