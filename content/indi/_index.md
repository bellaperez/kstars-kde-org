---
title: 'Devices supported under KStars'
menu:
  main:
    parent: res
    weight: 4
    identifier: indi
---

![](indi-welcome.png)

\[
  [Ekos](https://indilib.org/about/ekos.html) |
  [Telescopes](./telescopes/) |
  [CCDs](./ccds/) |
  [Focusers](./focusers/) |
  [Filters](./filters/) |
  [Others](./others/)
\]

### Overview
**KStars** supports a large number of astronomical instruments provided by the [INDI Library](http://www.indilib.org).
This includes [telescopes](./telescopes/), [CCDs](./ccds/), [focusers](./focusers/), 
[filters](./filters/), domes, spectrometers, and auxliary devices.
Using KStars native INDI support means you have complete control over your instruments under Linux.
In addition to astronomical devices support, KStars provides [Ekos](https://indilib.org/about/ekos.html), a complete tool for astrophotography. With Ekos, you can
align and guide your telescope, focus your CCD, and capture images using an easy intuitive interface. Furthermore, KStars FITSViewer tool
provides a simple viewer for FITS images that is capable of detecting stars and applying various filters to aid you in your astrophtography decisions.
Those features make KStars/INDI a very attractive choice for amateur and professional astronomers.
